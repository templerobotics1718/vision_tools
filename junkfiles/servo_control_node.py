#!/usr/bin/env python
import cv2
import numpy as np
import pantilthat
import rospy
from geometry_msgs.msg import Point
import tf

#initialize previous angles
prev_pan = 0
prev_tilt = 0

#initialize angles
pan_angle = 0
tilt_angle = 0

# tf broadcaster object
br = tf.TransformBroadcaster(queue_size=1000)

#function rotates the servos for the amount required to reach the desired angle
def pan_tilt(angles):

    #global variables
    global prev_pan
    global prev_tilt
    global pan_angle
    global tilt_angle

    # angle bounds
    MAX_BOUND = 89
    MIN_BOUND = -89

    #initialize pan and tilt angles
    pan_angle = angles.x
    tilt_angle = angles.y

    if (prev_tilt != tilt_angle): #check to see if there's new input
        if(tilt_angle > MAX_BOUND): # make sure its not out of bounds
            tilt_angle = MAX_BOUND
        elif(tilt_angle < MIN_BOUND): # make sure its not out of bounds
            tilt_angle = MIN_BOUND
        pantilthat.servo_two(int(tilt_angle)) # move camera
        prev_tilt = tilt_angle # set previous angle to check future input

    if(prev_pan != pan_angle): #check to see if there's new input
        if(pan_angle > MAX_BOUND): # make sure its not out of bounds
            pan_angle = MAX_BOUND
        elif(pan_angle < MIN_BOUND): # make sure its not out of bounds
            pan_angle = MIN_BOUND
        pantilthat.servo_one(int(pan_angle)) # move camera
        prev_pan = pan_angle # set previous angle to check future input

    # constantly update tf
    quaternion = tf.transformations.quaternion_from_euler(0,((tilt_angle*3.14)/180), ((-pan_angle*3.14)/180))
    br.sendTransform( (0.08, 0, 0.10), quaternion, rospy.Time.now(),"camera", "base_link")


        
        


def main():
    pantilthat.servo_one(0) #init position
    pantilthat.servo_two(0)
    rospy.init_node('servo_control_node',anonymous=True)
    rospy.Subscriber("servos", Point, pan_tilt)
    quaternion = tf.transformations.quaternion_from_euler(0,((0*3.14)/180), 0)
    br.sendTransform( (0.08, 0, 0.10), quaternion, rospy.Time.now(),"camera", "base_link")
    rospy.spin()

if __name__ == "__main__":
    main()

