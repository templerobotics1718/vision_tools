#!/usr/bin/env python

import roslib
import rospy
import tf
import pantilthat

br = tf.TransformBroadcaster(queue_size=1000)

def send_pose():
   quaternion = tf.transformations.quaternion_from_euler(0,((0*3.14)/180), 0)
   br.sendTransform( (0.08, 0, 0.10), quaternion, rospy.Time.now(),"camera", "base_link")

if __name__ == '__main__':
   rospy.init_node('visotest_tf_broadcaster')
   pantilthat.servo_one(-20)
   pantilthat.servo_two(0)
   while (not rospy.is_shutdown()):
      send_pose()

