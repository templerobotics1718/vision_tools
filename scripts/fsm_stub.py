#!/usr/bin/env python
import roslib
import rospy
import actionlib
import time
import vision_tools.msg

# this is a stub that simulates FSM functionality, which will start
# the initial correction step
#
if __name__ == "__main__":
  rospy.init_node('fsm_stub')
  cli = actionlib.SimpleActionClient('initial_correction', vision_tools.msg.FindChessboardAction)
  print "waiting for server..."
  cli.wait_for_server()
  print "sending goal..."
  goal = vision_tools.msg.FindChessboardGoal()
  cli.send_goal(goal)
  cli.wait_for_result()
  res = cli.get_result()
  print "received result {} from the server".format(res)
  
