#!/usr/bin/env python

import cv2
from cv_bridge import CvBridge
import numpy as np

import roslib
import rospy
import actionlib

from sensor_msgs.msg import Image
import vision_tools.msg

frame = np.zeros((0, 0, 0), np.uint8)

bridge = CvBridge()

def imageCallback(image):
  global frame
  frame = bridge.imgmsg_to_cv2(image, desired_encoding='passthrough')

# method to handle when opencv finds a chessboard
#
def foundChessboard():
  pass

class initial_correction:
  def __init__(self):
    rospy.loginfo("setting up initial_correction instance...")
    self.srv = actionlib.SimpleActionServer('initial_correction', vision_tools.msg.FindChessboardAction, self.callback, False)
    self.srv.start()
  def callback(self, goal):
    ts_cli = actionlib.SimpleActionClient('turn_stepper', vision_tools.msg.TurnStepperAction)
    ts_cli.wait_for_server()
    zs_cli = actionlib.SimpleActionClient('zero_stepper', vision_tools.msg.TurnStepperAction)
    zs_cli.wait_for_server()
    goal = vision_tools.msg.TurnStepperGoal()
    goal.id = 1
    rospy.loginfo("starting up stepper...")
    ts_cli.send_goal(goal)
    goal.id = 2
    print "starting up chessboard detection..."
    res = vision_tools.msg.FindChessboardResult()
    res.found_object = 100
    while (ts_cli.get_state() != 3) and (ts_cli.get_state() != 4):
      # begin code marked for deletion:  This should be replaced by pulling
      # code from Spencer's image publisher
      
      # end code marked for deletion
      # if frame is not None:
      coords = cv2.findChessboardCorners(frame, (8, 6), None)
      # if opencv found a chessboard make a call to foundChessboard
      #
      if coords[0]:
        rospy.loginfo("Found it!!!")
        #foundChessboard()
        res.found_object = 200
        ts_cli.send_goal(goal)
        zs_cli.send_goal(goal)
    print "done..."
    # release the webcam
    self.srv.set_succeeded(res)


if __name__ == "__main__":
  print "starting up..."
  rospy.init_node('find_chessboard')
  rospy.Subscriber('/cam1/image', Image, imageCallback)
  srv = initial_correction()
  # rospy.spin()
