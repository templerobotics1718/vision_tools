#!/usr/bin/env python

import roslib
import rospy
import actionlib

import stepper.msg
import time

class action_test:
  def __init__(self):
    self.srv = actionlib.SimpleActionServer('test', stepper.msg.MakeCorrectionAction, self.callback, False)
    self.srv.start()
  def callback(self, goal):
    print "doing important stuff..."
    #time.sleep(4)
    print "done"
    self.srv.set_succeeded()

if __name__ == "__main__":
  rospy.init_node('toy_server')
  srv = action_test()
  rospy.spin()
