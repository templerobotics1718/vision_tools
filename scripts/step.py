#!/usr/bin/env python

import sys
import time

# import the Stepper class from the Phidget python bindings
#
from Phidget22.Devices import Stepper

# handler for when the phidget is attached
#
def phidget_stepper_init(self):
  stepper = self
  print "Using device {}: {}".format(stepper.getDeviceID(),
    stepper.getDeviceName())

# create Stepper instance
#
ph_stepper = Stepper.Stepper()

# create global variable for the offset from the initial correction
#
offset = int()

def startup():
  # add handler for when the phidget is attached
  #
  ph_stepper.setOnAttachHandler(phidget_stepper_init)
  # try to find an attached phidget for 5 seconds
  #
  ph_stepper.setChannel(0)
  ph_stepper.openWaitForAttachment(5000)
  # engage the stepper motor
  #
  
  print "turning on motor 1"
  ph_stepper.setEngaged(1)


  print "setting velocity limit"
  ph_stepper.setVelocityLimit(20)

# method for rotating a stepper (angle is angular displacement)
#
def rotate_stepper(angle):
  # wait until we're done moving to move some more
  #
  while ph_stepper.getIsMoving():
    pass
  # move the stepper motor to the position given by angle
  #
  print "moving to position {}({})".format(angle, angle + offset)
  ph_stepper.setTargetPosition(angle + offset)
  # do nothing as long as the stepper is moving
  #
  while ph_stepper.getIsMoving():
    pass

def set_position_callback(callback):
  ph_stepper.setOnPositionChangeHandler(callback)


# method for zeroing the stepper (sets offset to whatever the current
# absolute position is)
#
def zero_stepper():
  global offset
  new_offset = int(ph_stepper.getPosition())
  print "Setting offset to absolute position {}".format(new_offset)
  offset = new_offset

if __name__ == "__main__":
  # rotate the stepper (use argv[1] as the argument)
  #
  startup()
  rotate_stepper(int(sys.argv[1]))
  pass
