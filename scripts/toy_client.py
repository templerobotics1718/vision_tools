#!/usr/bin/env python

import roslib
import rospy
import actionlib
import time
import stepper.msg
import os
import pdb

if __name__ == "__main__":
  print os.path.abspath(stepper.msg.__file__)
  rospy.init_node('toy_client')
  cli = actionlib.SimpleActionClient('test', stepper.msg.MakeCorrectionAction)
  print "waiting for server..."
  cli.wait_for_server()
  goal = stepper.msg.MakeCorrectionGoal()
  print "sending goal..."
  cli.send_goal(goal)
  #cli.wait_for_result(rospy.Duration.from_sec(10.0))
  for i in range(0,10):
    print cli.get_state()
    print cli.get_goal_status_text()
    time.sleep(1)
  #print "got result..."
