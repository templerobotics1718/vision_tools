#!/usr/bin/env python

import step

import roslib
import rospy
import actionlib

import vision_tools.msg

import time

from std_msgs.msg import Int32
from std_msgs.msg import Float32
from geometry_msgs.msg import Point

import numpy as np
import tf
import pantilthat

prev_pan = 0
prev_tilt = 0

pan_angle = 0
tilt_angie = 0

br = tf.TransformBroadcaster(queue_size=1000)

pub = rospy.Publisher('/stepper_angle', Point, queue_size=10)

# callback function for the position subscriber (moves to the desired
# pan angle)
#
#def pos_callback(data):
  #step.rotate_stepper(data.data)

def position_callback(self, position):
  p = Point()
  p.y = position
  
  pub.publish(p)

  # constantly update tf
  pan_angle = p.y
  tilt_angle = 0
  quaternion = tf.transformations.quaternion_from_euler(0,((tilt_angle*3.14)/180), ((pan_angle*3.14)/180))
  br.sendTransform( (-0.495, 0.165, 0.686), quaternion, rospy.Time.now(),"camera", "base_link")
  

def pan_step(angles):
  global prev_pan
  global pan_angle

  MAX_BOUND_PAN = 720
  MIN_BOUND_PAN = -720

  pan_angle = angles.x
  if(prev_pan != pan_angle): #check to see if there's new input
      if(pan_angle > MAX_BOUND_PAN): # make sure its not out of bounds
          pan_angle = MAX_BOUND_PAN
      elif(pan_angle < MIN_BOUND_PAN): # make sure its not out of bounds
          pan_angle = MIN_BOUND_PAN
      #pantilthat.servo_one(int(pan_angle)) # move camera
      step.rotate_stepper(int(pan_angle))
      prev_pan = pan_angle # set previous angle to check future input
  
def start_pos_sub():
  #rospy.Subscriber("pos_data", Int32, pos_callback)
  rospy.Subscriber("/stepper_pub", Point, pan_step)
# 
class stepper_ctl:
  def __init__(self):
    # we need different server instances here for concurrent action handling
    #
    print "starting up actionlib servers..."
    self.srv1 = actionlib.SimpleActionServer('turn_stepper', vision_tools.msg.TurnStepperAction, self.turn_callback, False)
    self.srv2 = actionlib.SimpleActionServer('zero_stepper', vision_tools.msg.TurnStepperAction, self.zero_callback, False)
    self.srv1.start()
    self.srv2.start()
    print "servers ready"
    step.startup()
  def turn_callback(self, goal):
    # goal.id == 1: do a 360 pan and back
    #
    if goal.id == 1:
      print "doing 360 pan..."
      step.rotate_stepper(360)
      print "moving back to 0"
      step.rotate_stepper(0)
      print "done..."
      self.srv1.set_succeeded()
    # goal.id == 2: start position subscriber
    #
    elif goal.id == 2:
      print "starting position subscriber..."
      start_pos_sub()
      self.srv1.set_succeeded()
  # zero_callback: zero the stepper motor
  #
  def zero_callback(self, goal):
    print "found chessboard! zeroing out motor..."
    step.zero_stepper()
    print "done..."
    self.srv2.set_succeeded()
      

if __name__ == "__main__":
  step.startup()
  print "starting up..."
  rospy.init_node('stepper_node')
  start_pos_sub()
  srv = stepper_ctl()
  step.set_position_callback(position_callback)
  rospy.spin()

