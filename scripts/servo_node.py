#!/usr/bin/env python

import roslib
import rospy
import actionlib

import vision_tools.msg

import time

from std_msgs.msg import Int32
from std_msgs.msg import Float32
from geometry_msgs.msg import Point

import numpy as np
import tf
import pantilthat

prev_pan = 0
prev_tilt = 0

pan_angle = 0
tilt_angie = 0

br = tf.TransformBroadcaster(queue_size=1000)

#pub = rospy.Publisher('/stepper_angle', Point, queue_size=10)

# callback function for the position subscriber (moves to the desired
# pan angle)
#
#def pos_callback(data):
  #step.rotate_stepper(data.data)

#def position_callback(self, position):
#  p = Point()
#  p.y = position
#  
#  pub.publish(p)

  # constantly update tf
#  pan_angle = p.y
#  tilt_angle = 0
#  quaternion = tf.transformations.quaternion_from_euler(0,((tilt_angle*3.14)/180), ((pan_angle*3.14)/180))
#  br.sendTransform( (-0.495, 0.165, 0.686), quaternion, rospy.Time.now(),"camera", "base_link")
  
def pan_tilt(angles):

  #global variables
  global tilt_angle
  global prev_tilt

  # angle bounds

  MAX_BOUND_TILT = 89
  MIN_BOUND_TILT = -89

  #initialize pan and tilt angles
  tilt_angle = angles.y

  if (prev_tilt != tilt_angle): #check to see if there's new input
      if(tilt_angle > MAX_BOUND_TILT): # make sure its not out of bounds
          tilt_angle = MAX_BOUND_TILT
      elif(tilt_angle < MIN_BOUND_TILT): # make sure its not out of bounds
          tilt_angle = MIN_BOUND_TILT
      print("tilt angle is: " + str(tilt_angle))
      pantilthat.servo_one(int(-tilt_angle)) # move camera
      prev_tilt = tilt_angle # set previous angle to check future input

      quaternion = tf.transformations.quaternion_from_euler(0,((tilt_angle*3.14)/180), ((0/180)))
      br.sendTransform( (-0.495, 0.165, 0.686), quaternion, rospy.Time.now(),"camera", "base_link")      

# function to start the position subscriber
#
  
def start_pos_sub():
  #rospy.Subscriber("pos_data", Int32, pos_callback)
  rospy.Subscriber("/servos", Point, pan_tilt)


if __name__ == "__main__":
  pantilthat.servo_one(0)
  print "starting up..."
  rospy.init_node('servo_node')
  start_pos_sub()
  #srv = stepper_ctl()
  rospy.spin()

