#!/usr/bin/env python

import math
from time import sleep

import rospy
import cv2
import cv_bridge
import cv2.aruco as aruco
import numpy as np

from geometry_msgs.msg import Point
from sensor_msgs.msg import Image
from std_msgs.msg import Int8
from std_msgs.msg import Float32

# ROS Publishers
pub = rospy.Publisher('stepper_pub', Point, queue_size=0)
laser_pub = rospy.Publisher('laser_range_confirmation', Int8, queue_size=10)

# Constants
frame_w = 640
frame_h = 480
fov = 45
min_angle_change = 5

# Aruco Data
aruco_dict =  aruco.getPredefinedDictionary(aruco.DICT_6X6_250)
aruco_parameters = aruco.DetectorParameters_create()

# CV Bridge
bridge = cv_bridge.CvBridge()

# Globals
image = np.array([])
pan_angle = 0

def image_callback(image_msg):
    global image
    image = bridge.imgmsg_to_cv2(image_msg, desired_encoding='passthrough')

def angle_callback(angle):
    global pan_angle
    #rospy.loginfo(pan_angle)
    pan_angle = angle.y 
    #print("Pan angle: " + str(pan_angle))

def get_aruco_position(frame):
    try:
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    except Exception as error:
        return (None, None)
    blurred = cv2.GaussianBlur(gray, (7, 7), 0)
    edged = cv2.Canny(blurred, 50, 150)

    # find contours in the edge map
    val1, cnts, val2 = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    narray, ids, _= aruco.detectMarkers(gray, aruco_dict, parameters=aruco_parameters)
    try:
        corners = []
        for i in range(len(ids)):
            if not narray:
                print("corner list is not available")

        corners.append(narray[0].tolist())

        centerX = (frame_w) / 2
        centerY = (frame_h) / 2
        
    # compute the center of the aruco region
    # remember, the first ordered pair in corners is the first closest point from the origin (top-left corner), assuming there's one aruco image rather than multiple.
    # p1 ------ p2
    # |         |
    # |         |
    # p4 ------ p3
      
        dx = ((corners[0][0][2][0] + corners[i][0][0][0])/2)
        dy = ((corners[0][0][2][1] + corners[i][0][0][1])/2)

        dX = int(dx)
        dY = int(dy)
	
	laser_pub.publish(1)

        
        return (dX, dY)

    except Exception as error:
        print("No aruco is detected")
	laser_pub.publish(0)
        return (None, None)

def get_relative_angles(x_pos, y_pos):
    offset_x = x_pos - (frame_w / 2)
    offset_y = y_pos - (frame_h / 2)

    percent_x = float(offset_x) / frame_w
    percent_y = float(offset_y) / frame_h

    angle_x = percent_x * fov
    angle_y = percent_y * fov

    return (-angle_x, -angle_y)

def get_absolute_angles(angle_x, angle_y):
    angle_x = pan_angle + angle_x
    angle_y = angle_y

    return (angle_x, angle_y)

def update():
    global image
    [x_pos, y_pos] = get_aruco_position(image)
    if (x_pos is not None and y_pos is not None):
        [d_angle_x, d_angle_y] = get_relative_angles(x_pos, y_pos)
        if (abs(d_angle_x) > min_angle_change or abs(d_angle_y > min_angle_change)):
            [angle_x, angle_y] = get_absolute_angles(d_angle_x, d_angle_y)
	    
            point_message = Point()
            point_message.x = angle_x
            point_message.y = 0
            print("dX: " + str(d_angle_x))
            print(" X: " + str(angle_x))

            #once it reaches this line of code
            #we already assume that we see the aruco in line of sight
            pub.publish(point_message)


            
def main():

    rospy.init_node('pan', anonymous=True)
    rate = rospy.Rate(4)
    rospy.Subscriber('/cam1/image', Image, image_callback)
    rospy.Subscriber('/stepper_angle', Point, angle_callback)
        
    while not rospy.is_shutdown():
        update()
        rate.sleep()
        
        

if __name__ == "__main__":
    main()
