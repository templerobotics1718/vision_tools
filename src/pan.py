#!/usr/bin/env python
import rospy
import cv2
from geometry_msgs.msg import Point
from sensor_msgs.msg import Image
from std_msgs.msg import Int8
from time import sleep
import cv_bridge
import cv2.aruco as aruco
import math
pub = rospy.Publisher('/servos', Point, queue_size=10)
laser_pub = rospy.Publisher('laser_range_confirmation', Int8, queue_size=10)


##################################################################
# 640 x 480 resolution
# Therefore, center of frame is 320 x 240
frame_w = 640
frame_h = 480
fov = 170

rotation_scale = 0.02
aruco_dict =  aruco.getPredefinedDictionary(aruco.DICT_6X6_250)
parameters = aruco.DetectorParameters_create()
theta_x_limit = 89
theta_y_limit = 89

deadzone_percent = 0.075

theta_x = 0
theta_y = 0

def callback(image):

    global theta_x
    global theta_y
    
    bridge = cv_bridge.CvBridge()
    p = Point()
    frame = bridge.imgmsg_to_cv2(image, desired_encoding='passthrough')

    
    # convert the frame to grayscale, blur it, and detect edges
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (7, 7), 0)
    edged = cv2.Canny(blurred, 50, 150)

    # find contours in the edge map
    val1, cnts, val2 = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    narray, ids, _= aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    try:
        corners = []
        for i in range(len(ids)):
            if not narray:
                print("corner list is not available")

        corners.append(narray[0].tolist())
   # cv2.drawContours(frame, [approx], 0, (255, 255, 0), 3)

        centerX = (frame_w) / 2
        centerY = (frame_h) / 2
        
    # compute the center of the aruco region
    # remember, the first ordered pair in corners is the first closest point from the origin (top-left corner), assuming there's one aruco image rather than multiple.
    # p1 ------ p2
    # |         |
    # |         |
    # p4 ------ p3
      
        dx = ((corners[0][0][2][0] + corners[i][0][0][0])/2)
        dy = ((corners[0][0][2][1] + corners[i][0][0][1])/2)

        dX = int(dx)
        dY = int(dy)
        #cv2.circle(frame, (dX, dY), 13, (0,0,255), -1)
        #cv2.imshow('frame', frame)
        #cv2.waitKey(40)
        #uncomment this code if you want to use multiple arucos. 
        #dist.append(math.sqrt((dX - centerX)**2 + (dY - centerY)**2))
        #index = min(dist)

                
        # correct center of aruco marker relative to center of image
        offset_x = dX - (frame_w / 2)
        offset_y = dY - (frame_h / 2)

        # convert to percentage offset on range from -0.5 to 0.5
        percent_x = offset_x / float(frame_w)
        percent_y = offset_y / float(frame_h)

        # scale offset to degrees
        delta_theta_x = percent_x * fov * rotation_scale
        delta_theta_y = percent_y * fov * rotation_scale


        # print angle change for test
        print(str(delta_theta_x) + ", " + str(delta_theta_y))

        # Check to see if the angles are outside a "deadzone" to prevent jitter on close angles
        if (abs(percent_x) > deadzone_percent):
            print("Yaw outside deadzone")
            theta_x += delta_theta_x

        # Limit angle to range -theta_limit to theta_limit
        theta_x = max(-theta_x_limit, theta_x)
        theta_x = min(theta_x_limit, theta_x)
        if (abs(percent_y) > deadzone_percent):
            print("Pitch outside deadzone")
            theta_y += delta_theta_y
            theta_y = max(-theta_y_limit, theta_y)
            theta_y = min(theta_y_limit, theta_y)

    
        p.x = theta_x
        p.y = theta_y 

    # publish servo angles
        pub.publish(p)
        laser_pub.publish(1)

    # Sleep to wait for angle to update
        sleep(0.01875)

    except Exception as error:
        laser_pub.publish(0)
        print("No aruco is detected")



def pubsub():
    rospy.init_node('pan', anonymous=True)
    rospy.Subscriber('/cam1/image', Image, callback)
    rospy.spin()


if __name__ == '__main__':
    try:
        pubsub()
    except rospy.ROSInterruptException:
        pass
