#!/usr/bin/env python

import math
from time import sleep

import rospy
import cv2
import cv_bridge
import cv2.aruco as aruco
import numpy as np

from geometry_msgs.msg import Point
from sensor_msgs.msg import Image
from std_msgs.msg import Int8
from std_msgs.msg import Float32

# ROS Publishers
pub = rospy.Publisher('/servos', Point, queue_size=0)
laser_pub = rospy.Publisher('laser_range_confirmation', Int8, queue_size=10)

# Constants
frame_w = 640
frame_h = 480
fov = 45
min_angle_change = 5

# Aruco Data
aruco_dict =  aruco.getPredefinedDictionary(aruco.DICT_6X6_250)
aruco_parameters = aruco.DetectorParameters_create()

# CV Bridge


# Globals
pan_angle = 0
theta_y = 0
theta_y_limit = 89

def get_aruco_position(frame):
    global theta_y
    bridge = cv_bridge.CvBridge()
    image = bridge.imgmsg_to_cv2(frame, desired_encoding='passthrough')
    points = Point()
    
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    blurred = cv2.GaussianBlur(gray, (7, 7), 0)
    edged = cv2.Canny(blurred, 50, 150)

    # find contours in the edge map
    val1, cnts, val2 = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    narray, ids, _= aruco.detectMarkers(gray, aruco_dict, parameters=aruco_parameters)
    try:
        corners = []
        for i in range(len(ids)):
            if not narray:
                print("corner list is not available")

        corners.append(narray[0].tolist())

        centerX = (frame_w) / 2
        centerY = (frame_h) / 2
        
    # compute the center of the aruco region
    # remember, the first ordered pair in corners is the first closest point from the origin (top-left corner), assuming there's one aruco image rather than multiple.
    # p1 ------ p2
    # |         |
    # |         |
    # p4 ------ p3
      
        dx = ((corners[0][0][2][0] + corners[i][0][0][0])/2)
        dy = ((corners[0][0][2][1] + corners[i][0][0][1])/2)

        dX = int(dx)
        dY = int(dy)

        offset_y = dY - (frame_h / 2)
	
	percent_y = offset_y / float(frame_h)
        delta_theta_y = percent_y * 170 * 0.075

        if(abs(percent_y) > 0.075):
            theta_y += delta_theta_y
            theta_y = max(-theta_y_limit, theta_y)
    	    theta_y = min(theta_y_limit, theta_y)

        points.x = 0
        print(theta_y)
        points.y = theta_y

        pub.publish(points)
            
    except Exception as error:
        print("No aruco is detected")
	laser_pub.publish(0)

def main():

    rospy.init_node('pan_servo', anonymous=True)
    rospy.Subscriber('/cam1/image', Image, get_aruco_position)
    rospy.spin()     
        

if __name__ == "__main__":
    main()
