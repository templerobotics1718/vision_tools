#include "image_publisher.h"

#include <ros/console.h>

using namespace turmc;

ImagePublisher::~ImagePublisher()
{
  release();
}

bool ImagePublisher::init(image_transport::ImageTransport &it, unsigned int queue_size)
{
  // Try to create a new image mesage node
  try {
    _pub = it.advertiseCamera(TOPIC_PREFIX + _params.name + TOPIC_SUFFIX, queue_size);
  } catch (ros::InvalidNameException &e) {
    ROS_ERROR_STREAM("Invalid publisher name " << TOPIC_PREFIX << _params.name << TOPIC_SUFFIX);
    return false;
  }
  ROS_INFO_STREAM("Publishing image on " << TOPIC_PREFIX << _params.name << TOPIC_SUFFIX);

  // Try to open video capture device using V4L paths
  if (!_capture.open(_params.device_name, cv::CAP_V4L))
    return false;

  _initialized = true;
  return true;
}

bool ImagePublisher::release()
{
  if (!_initialized)
    return false;

  // Release video device and reset value
  _capture.release();
  _initialized = false;
  return true;
}

bool ImagePublisher::publish()
{
  if (!_initialized)
  {
    ROS_WARN_STREAM("Attempting to publish uninitialized image_publisher");
    return false;
  }

  if (!_capture.read(_frame))
  {
    ROS_WARN_STREAM("Unable to read image from publisher " << _params.name);
    return false;
  }

  sensor_msgs::ImagePtr img;

  if (_params.use_calibration)
  {
    cv::undistort(_frame, _rectified_frame, cv::Mat(3, 3, CV_64F, _params.K), _params.D);
    img = cv_bridge::CvImage(std_msgs::Header(), "bgr8", _rectified_frame).toImageMsg();
  }
  else
    img = cv_bridge::CvImage(std_msgs::Header(), "bgr8", _frame).toImageMsg();

  sensor_msgs::CameraInfoPtr ci(new sensor_msgs::CameraInfo(turmc::getInfoFromParams(_params)));
  img->header.stamp = ci->header.stamp;
  _pub.publish(img, ci);

  return true;
}
