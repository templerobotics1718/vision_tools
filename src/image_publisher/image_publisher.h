#pragma once

#include <string>
#include <vector>
#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <std_msgs/String.h>
#include <image_transport/image_transport.h>

#include "camera_params.h"

#define TOPIC_PREFIX "/"
#define TOPIC_SUFFIX "/image"

namespace turmc
{
  class ImagePublisher
  {
  private:
    bool _initialized;
    CameraParams _params;
    image_transport::CameraPublisher _pub;
    cv::VideoCapture _capture;
    cv::Mat _frame;
    cv::Mat _rectified_frame;

  public:
    ImagePublisher(CameraParams params) : _initialized(false), _params(params) {};
    ~ImagePublisher();

    bool init(image_transport::ImageTransport &it, unsigned int queue_size);
    bool release();
    bool publish();
  };
}
