#include <vector>
#include <ros/ros.h>
#include <std_msgs/String.h>

#include "image_publisher.h"
#include "camera_params.h"

int main(int argc, char** argv)
{
  // Initialize ROS "image_publisher" node
  ros::init(argc, argv, "image_publisher");

  // Get image transport handler
  ros::NodeHandle nh;
  image_transport::ImageTransport it(nh);

  // Create lists for parameters and publishers
  XmlRpc::XmlRpcValue camera_list;
  std::vector<turmc::ImagePublisher> publishers;

  // Exit of /video_devices parameter doesn't exist
  if (!nh.getParam("/video_devices", camera_list))
  {
    ROS_ERROR("Unable to get /video_devices parameter");
    return -1;
  }

  for (auto i = camera_list.begin(); i != camera_list.end(); i++)
  {
    // Load camera info from parameter server
    turmc::CameraParams camera_params = turmc::loadCameraParams(i->first, i->second);

    // Only check for valid device_name - not all nodes need other camera data
    if (camera_params.device_name.length() > 0)
    {
      // Construct publisher in place (Probably not necessary)
      auto new_pub = publishers.emplace(publishers.end(), camera_params);

      // Initialize image publisher with device_name as property value, and publisher name as property name
      if (!new_pub->init(it, 5))
      {
        // On failure warn and remove from list
        publishers.erase(new_pub);
        ROS_WARN_STREAM("Failed to create image publisher " << camera_params.name << " on " << camera_params.device_name);
      }
      else
        ROS_INFO_STREAM("Image publisher " << camera_params.name << " created at " << camera_params.device_name);
    }
  }

  // Publish at 30 Hz
  ros::Rate pub_rate(30);

  // While the node is running
  while(nh.ok())
  {
    // Publish each device's latest image
    for (auto i = publishers.begin(); i != publishers.end(); i++)
    {
      i->publish();
    }

    // Wait for next cycle
    ros::spinOnce();
    pub_rate.sleep();
  }

  return 0;
}
