/* TODO: Add correct matrix length checking
 */

#include "camera_params.h"

// Gets parameter by name and stores to result
// Sends a ROS warning or debug info based on warn
// Message contains name and type_str if appropriate
// Casts to T_int and then T
// Checks using type

void getMatrixParam(double* result, XmlRpc::XmlRpcValue& params, std::string param_name, unsigned int length, std::string name)
{
  if (params.hasMember(param_name))
  {
    XmlRpc::XmlRpcValue temp = params[param_name];
    if (temp.getType() == XmlRpc::XmlRpcValue::Type::TypeArray)
    {
      if (temp.size() == length)
      {
        for (unsigned int i = 0; i < length; i ++)
        {
          if (temp[i].getType() == XmlRpc::XmlRpcValue::Type::TypeDouble)
            result[i] = (double)temp[i];
          else if (temp[i].getType() == XmlRpc::XmlRpcValue::Type::TypeInt)
            result[i] = (double)((int)temp[i]);
          else
          {
            result[i] = 0.0;
            ROS_WARN_STREAM("Invalid data type in " << name << "." << param_name << " - matrix may be malformed");
          }
        }
      }
      else
      {
        ROS_WARN_STREAM("Invalid matrix " << name << "." << param_name << " - incorrect matrix length");
      }
    }
  }
  else
    ROS_WARN_STREAM(name << " does not have a specified " << param_name << " matrix");
}

template <typename T, typename T_int = T> bool getParamAs(T& result, XmlRpc::XmlRpcValue& params,
                                      std::string param_name, XmlRpc::XmlRpcValue::Type type,
                                      std::string name, std::string type_str, bool warn = false)
{
  std::string msg;

  if (!params.hasMember(param_name))
  {
    msg = name + " does not have property " + param_name;

    if (warn)
      ROS_WARN_STREAM(msg);
    else
      ROS_INFO_STREAM(msg);

    return false;
  }

  auto temp = params[param_name];

  if (temp.getType() != type)
  {
    msg = name + "." + param_name + " is not of type " + type_str;

    if (warn)
      ROS_WARN_STREAM(msg);
    else
      ROS_INFO_STREAM(msg);

    return false;
  }

  // Cast using T_int (XmlRpcValue can only be cast to specific values - we can then cast these to other values)
  result = static_cast<T>((T_int)temp);
  return true;
}

turmc::CameraParams turmc::loadCameraParams(std::string name, XmlRpc::XmlRpcValue& param_list) {
  auto param_loc = param_list.end();
  CameraParams camera_params;

  camera_params.name = name;

  // Look for parameters by name, if they aren't found, warn
  getParamAs<std::string>(camera_params.device_name, param_list, "device_name",
                          XmlRpc::XmlRpcValue::Type::TypeString, name, "string", true);

  getParamAs<bool, int>(camera_params.use_calibration, param_list, "use_calibration",
                          XmlRpc::XmlRpcValue::Type::TypeInt, name, "int", true);

  if (camera_params.use_calibration)
  {
    getParamAs<std::string>(camera_params.frame_id, param_list, "frame_id",
                            XmlRpc::XmlRpcValue::Type::TypeString, name, "string");

    getParamAs<uint32_t, int>(camera_params.width, param_list, "width",
                            XmlRpc::XmlRpcValue::Type::TypeInt, name, "int");

    getParamAs<uint32_t, int>(camera_params.height, param_list, "height",
                            XmlRpc::XmlRpcValue::Type::TypeInt, name, "int");

    getParamAs<std::string>(camera_params.distortion_model, param_list, "distortion_model",
                            XmlRpc::XmlRpcValue::Type::TypeString, name, "string");

    if (param_list.hasMember("D"))
    {
      XmlRpc::XmlRpcValue temp = param_list["D"];
      if (temp.getType() == XmlRpc::XmlRpcValue::Type::TypeArray)
      {
        for (unsigned int i = 0; i < temp.size(); i++)
        {
          if (temp[i].getType() == XmlRpc::XmlRpcValue::Type::TypeDouble)
            camera_params.D.push_back((double)temp[i]);
          else if (temp[i].getType() == XmlRpc::XmlRpcValue::Type::TypeInt)
            camera_params.D.push_back((double)((int)temp[i]));
          else
            ROS_WARN_STREAM("Invalid data type in " << name << ".D - distortion parameters may be malformed");
        }
      }
      else
        ROS_WARN_STREAM(name << ".D is not a valid array");
    }
    else
      ROS_INFO_STREAM(name << " does not have distortion parameters");

    getMatrixParam(camera_params.K, param_list, "K", 9, name);
    getMatrixParam(camera_params.R, param_list, "R", 9, name);
    getMatrixParam(camera_params.P, param_list, "P", 12, name);
  }

  return camera_params;
}

sensor_msgs::CameraInfo turmc::getInfoFromParams(CameraParams params) {
  sensor_msgs::CameraInfo ci;
  unsigned char i;

  ci.header.stamp = ros::Time::now();

  if (params.use_calibration)
  {
    ci.header.frame_id = params.frame_id;
    ci.height = params.height;
    ci.width = params.width;
    ci.distortion_model = params.distortion_model;
    ci.D = params.D;
    for (i = 0; i < 9; i++) {
      ci.K[i] = params.K[i];
    }
    for (i = 0; i < 9; i++) {
      ci.R[i] = params.R[i];
    }
    for (i = 0; i < 12; i++) {
      ci.P[i] = params.P[i];
    }
    ci.binning_x = 0;
    ci.binning_y = 0;
    ci.roi.x_offset = 0;
    ci.roi.y_offset = 0;
    ci.roi.height = 0;
    ci.roi.width = 0;
    ci.roi.do_rectify = false;
  }

  return ci;
}
