#pragma once

#include <string>
#include <map>
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>

namespace turmc
{
  // Should correspond to sample YAML file
  // Somewhat same as camera_info message type
  struct CameraParams {
    std::string name;
    std::string device_name;
    bool use_calibration = false;
    std::string frame_id = "0";
    uint32_t height = 0;
    uint32_t width = 0;
    std::string distortion_model = "";
    std::vector<double> D;
    double K[9];
    double R[9];
    double P[12];
  };

  CameraParams loadCameraParams(std::string name, XmlRpc::XmlRpcValue& param_list);
  sensor_msgs::CameraInfo getInfoFromParams(CameraParams _params);
}
